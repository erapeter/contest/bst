module contest/bst

go 1.15

require (
	github.com/francoispqt/onelog v0.0.0-20190306043706-8c2bb31b10a4
	github.com/gin-gonic/gin v1.6.3
)
