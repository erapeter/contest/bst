package main

import (
	"net/http"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"

	"contest/bst/api/btree"

	"contest/bst/api"

	"github.com/francoispqt/onelog"
	"github.com/gin-gonic/gin"
)

func main() {

	onelog.MsgKey("msg")

	logger := onelog.
		New(os.Stdout, onelog.INFO|onelog.WARN|onelog.ERROR|onelog.FATAL).
		With(func(e onelog.Entry) {
			// time
			e.String("ts", time.Now().Format(time.RFC3339Nano))
			// caller
			_, file, line, _ := runtime.Caller(3)
			split := strings.SplitAfter(file, "btree/")
			if len(split) == 2 {
				file = split[1]
			}
			e.String("caller", strings.Join([]string{file, strconv.Itoa(line)}, ":"))
		})

	gin.SetMode(gin.ReleaseMode)
	router := gin.New()

	bst := router.Group("/")
	bst.Use(api.Logger(logger))
	bst.Use(gin.Recovery())

	bt, err := btree.InitTree()
	if err != nil {
		logger.Error(err.Error())
		return
	}

	endpoint := api.NewEndpoint(btree.NewLogger(bt, logger))
	endpoint.AddHandlers(bst)

	http.ListenAndServe(":80", router)
}
