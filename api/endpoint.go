package api

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// Endpoint http доступ к репозиторию
type Endpoint struct {
	btree Structurer
}

// NewEndpoint конструктор эндпоинта
func NewEndpoint(
	btree Structurer,
) *Endpoint {
	return &Endpoint{
		btree: btree,
	}
}

// AddHandlers привязка обработчиков к Router
func (s *Endpoint) AddHandlers(router gin.IRoutes) {
	router.POST("insert", s.insert)
	router.GET("search", s.search)
	router.DELETE("delete", s.delete)
}

func (s *Endpoint) responseError(c *gin.Context, err error) {
	ginError := c.Error(err)
	c.AbortWithStatusJSON(http.StatusInternalServerError, ginError.JSON())
}

func (s *Endpoint) insert(c *gin.Context) {
	req := requestValues{}

	if err := c.ShouldBindJSON(&req); err != nil {
		s.responseError(c, err)
		return
	}

	s.btree.Insert(req.Value)

	c.Status(http.StatusOK)

}

func (s *Endpoint) search(c *gin.Context) {
	val, err := strconv.Atoi(c.Query("val"))
	if err != nil {
		s.responseError(c, err)
		return
	}

	if err := s.btree.Search(val); err != nil {
		s.responseError(c, err)
		return
	}

	c.Status(http.StatusOK)
}

func (s *Endpoint) delete(c *gin.Context) {
	val, err := strconv.Atoi(c.Query("val"))
	if err != nil {
		s.responseError(c, err)
		return
	}

	if err := s.btree.Delete(val); err != nil {
		s.responseError(c, err)
		return
	}

	c.Status(http.StatusOK)
}

type requestValues struct {
	Value int `json:"val"`
}
