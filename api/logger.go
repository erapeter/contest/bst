package api

import (
	"time"

	"github.com/francoispqt/onelog"
	"github.com/gin-gonic/gin"
)

// Logger слой логигирования gin 
func Logger(logger *onelog.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {

		start := time.Now()
		path := c.Request.URL.Path
		host := c.Request.Host
		raw := c.Request.URL.RawQuery

		c.Next()

		timeStamp := time.Now()
		latency := timeStamp.Sub(start)

		clientIP := c.ClientIP()
		method := c.Request.Method
		statusCode := c.Writer.Status()
		handlerName := c.HandlerName()

		errors := c.Errors.ByType(gin.ErrorTypeAny)
		errorMessage := errors.String()

		bodySize := c.Writer.Size()

		if raw != "" {
			path = path + "?" + raw
		}

		var chain onelog.ChainEntry
		switch true {
		case statusCode >= 400 && statusCode < 500:
			chain = logger.WarnWith(errorMessage)
		case statusCode >= 500:
			chain = logger.ErrorWith(errorMessage)
		default:
			chain = logger.InfoWith(errorMessage)
		}

		chain.
			String("took", latency.String()).
			String("ip", clientIP).
			String("host", host).
			String("method", method).
			String("path", path).
			String("handler", handlerName).
			Int("status", statusCode).
			Int("send-size", bodySize).
			Write()
	}
}
