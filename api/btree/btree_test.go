package btree

import (
	"reflect"
	"testing"
)

func TestInsert(t *testing.T) {
	r := &Root{}
	values := [5]int{2, 3, 5, 1, 1}

	for _, v := range values {
		r.Insert(v)
	}

	result := [5]int{}
	printArray(r.tr, &result, 0)

	wantResult := [5]int{1, 5, 3, 2}

	if !reflect.DeepEqual(result, wantResult) {
		t.Errorf(
			"expected struct %#v,\n result - %#v",
			result, wantResult,
		)
	}

}

func TestSearch(t *testing.T) {

	testCase := []struct {
		desc    string
		value   int
		wantErr string
	}{
		{
			desc:    "Элемент найден №1",
			value:   5,
			wantErr: "",
		},
		{
			desc:    "Элемент найден №2",
			value:   1,
			wantErr: "",
		},
		{
			desc:    "Элемент не найден",
			value:   13,
			wantErr: ErrorNodeNotFound.Error(),
		},
	}

	r := &Root{}
	values := [5]int{2, 3, 5, 1, 4}

	for _, v := range values {
		r.tr = addNode(r.tr, v)
	}

	for _, tc := range testCase {
		t.Logf(
			"Name test: %s",
			tc.desc,
		)

		err := r.Search(tc.value)

		if tc.wantErr == "" && err != nil {
			t.Fatalf("unexpected error: %v", err)
		}

		if tc.wantErr != "" &&
			(err == nil || tc.wantErr != err.Error()) {
			t.Fatalf(
				"want error %v got %v",
				tc.wantErr, err,
			)
		}
	}
}

func TestDelete(t *testing.T) {

	testCase := []struct {
		desc       string
		value      int
		values     [5]int
		wantValues [5]int
		wantErr    string
	}{
		{
			desc:       "Удалён элемент №1",
			value:      5,
			values:     [5]int{2, 3, 5, 1, 4},
			wantValues: [5]int{1, 4, 3, 2},
			wantErr:    "",
		},
		{
			desc:       "Удалён элемент №2",
			value:      3,
			values:     [5]int{2, 3, 5, 1, 4},
			wantValues: [5]int{1, 4, 5, 2},
			wantErr:    "",
		},
		{
			desc:       "Удалён элемент №3",
			value:      2,
			values:     [5]int{2},
			wantValues: [5]int{},
			wantErr:    "",
		},
		{
			desc:    "Элемент не найден",
			value:   13,
			wantErr: ErrorNodeNotFound.Error(),
		},
	}

	for _, tc := range testCase {
		t.Logf(
			"Name test: %s",
			tc.desc,
		)

		r := &Root{}

		for _, v := range tc.values {
			r.tr = addNode(r.tr, v)
		}

		err := r.Delete(tc.value)

		if tc.wantErr == "" && err != nil {
			t.Fatalf("unexpected error: %v", err)
		}

		if tc.wantErr != "" &&
			(err == nil || tc.wantErr != err.Error()) {
			t.Fatalf(
				"want error %v got %v",
				tc.wantErr, err,
			)
		}

		result := [5]int{}
		printArray(r.tr, &result, 0)

		if !reflect.DeepEqual(result, tc.wantValues) {
			t.Errorf(
				"expected struct %#v,\n result - %#v",
				result, tc.wantValues,
			)
		}
	}
}

// вывод дерева без дубликатов
func printArray(tr *tree, result *[5]int, idx int) int {

	if tr.left != nil {
		idx = printArray(tr.left, result, idx)
	}
	if tr.right != nil {
		idx = printArray(tr.right, result, idx)
	}

	result[idx] = tr.value

	return idx + 1
}
