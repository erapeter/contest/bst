package btree

import (
	"time"

	"contest/bst/api"

	"github.com/francoispqt/onelog"
)

type logger struct {
	next   api.Structurer
	logger *onelog.Logger
}

// NewLogger - слой логирования для работы с деревом
func NewLogger(
	next api.Structurer,
	l *onelog.Logger,
) api.Structurer {
	return &logger{
		next:   next,
		logger: l,
	}
}

func (l logger) Insert(value int) {

	begin := time.Now()
	l.logger.InfoWithFields("Start method", func(entry onelog.Entry) {
		entry.String("method", "Insert")
	})

	l.next.Insert(value)

	l.logger.InfoWithFields("Finish method", func(entry onelog.Entry) {
		entry.String("method", "Insert")
		entry.String("took", time.Since(begin).String())
	})

}

func (l logger) Search(value int) (err error) {

	begin := time.Now()
	l.logger.InfoWithFields("Start method", func(entry onelog.Entry) {
		entry.String("method", "Search")
	})

	err = l.next.Search(value)
	switch err {
	case nil:
		l.logger.InfoWithFields("Finish method", func(entry onelog.Entry) {
			entry.String("method", "Search")
			entry.String("took", time.Since(begin).String())
		})
	default:
		l.logger.ErrorWithFields("Finish method", func(entry onelog.Entry) {
			entry.String("method", "Search")
			entry.String("took", time.Since(begin).String())
			entry.Err("err", err)
		})
	}

	return err
}
func (l logger) Delete(value int) (err error) {

	begin := time.Now()
	l.logger.InfoWithFields("Start method", func(entry onelog.Entry) {
		entry.String("method", "Delete")
	})

	err = l.next.Delete(value)

	switch err {
	case nil:
		l.logger.InfoWithFields("Finish method", func(entry onelog.Entry) {
			entry.String("method", "Delete")
			entry.String("took", time.Since(begin).String())
		})
	default:
		l.logger.ErrorWithFields("Finish method", func(entry onelog.Entry) {
			entry.String("method", "Delete")
			entry.String("took", time.Since(begin).String())
			entry.Err("err", err)
		})
	}

	return err
}
