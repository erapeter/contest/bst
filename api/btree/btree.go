package btree

import (
	"encoding/json"
	"errors"
	"os"
)

// ErrorNodeNotFound - ...
var ErrorNodeNotFound = errors.New("node is not found")

// Root - корень дерева
type Root struct {
	tr *tree
}

type tree struct {
	value int
	count int
	left  *tree
	right *tree
}

// InitTree - возвращает дерево
func InitTree() (r *Root, err error) {
	f, err := os.Open("api/btree/init.json")
	if err != nil {
		return
	}
	defer f.Close()

	values := [30]int{}
	err = json.NewDecoder(f).Decode(&values)
	if err != nil {
		return
	}

	r = &Root{}

	for _, v := range values {
		r.tr = addNode(r.tr, v)
	}

	return
}

// Insert - добавление узлов в дерево
func (r *Root) Insert(value int) {
	r.tr = addNode(r.tr, value)
}

// Search - поиск узла в дереве
func (r *Root) Search(value int) error {
	return lookupNode(r.tr, value)
}

// Delete - удаление узла из дерева
func (r *Root) Delete(value int) (err error) {

	r.tr, err = deleteNode(r.tr, value)
	if err != nil {
		return
	}

	return
}

func addNode(tr *tree, value int) *tree {
	if tr == nil {
		return &tree{value, 1, nil, nil}
	}

	if tr.value == value {
		tr.count++
		return tr
	}

	if value < tr.value {
		tr.left = addNode(tr.left, value)
		return tr
	}
	tr.right = addNode(tr.right, value)

	return tr
}

func lookupNode(tr *tree, value int) (err error) {
	if tr == nil {
		return ErrorNodeNotFound
	}

	if tr.value == value {
		return nil
	}

	if value < tr.value {
		return lookupNode(tr.left, value)
	}

	return lookupNode(tr.right, value)
}

func deleteNode(tr *tree, value int) (t *tree, err error) {
	if tr == nil {
		return tr, ErrorNodeNotFound
	}

	if value < tr.value {
		tr.left, err = deleteNode(tr.left, value)
	} else if value > tr.value {
		tr.right, err = deleteNode(tr.right, value)
	} else if tr.right != nil && tr.left != nil {
		min := minimum(tr.right)
		tr.value = min.value
		tr.count = min.count
		tr.right, err = deleteNode(tr.right, tr.value)
	} else {
		if tr.left != nil {
			tr = tr.left
		} else if tr.right != nil {
			tr = tr.right
		} else {
			tr = nil
		}
	}

	return tr, err
}

func minimum(tr *tree) *tree {
	if tr.left == nil {
		return tr
	}
	return minimum(tr.left)
}
