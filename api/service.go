package api

// Structurer - интерфейс для работы со структурами
type Structurer interface {
	Insert(value int)
	Search(value int) error
	Delete(value int) error
}
